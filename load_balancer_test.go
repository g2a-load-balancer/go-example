package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_LoadUnderStrategy(t *testing.T) {
	s := NewLoadUnderStrategy(0.75)

	t.Run("SelectFirstWithLoadUnder0.75", func(t *testing.T) {
		hosts := generateHostMocks(0.8, 0.8, 0.7, 0.9)
		host, err := s.SelectHost(hosts)
		assert.NoError(t, err)
		assert.Same(t, hosts[2], host)
	})

	t.Run("SelectWithTheLowestLoad", func(t *testing.T) {
		hosts := generateHostMocks(0.9, 0.9, 0.9, 0.8)
		host, err := s.SelectHost(hosts)
		assert.NoError(t, err)
		assert.Same(t, hosts[3], host)
	})

	t.Run("ErrorOnEmptyList", func(t *testing.T) {
		_, err := s.SelectHost([]Host{})
		assert.ErrorIs(t, err, ErrEmptyListOfHosts)
	})
}

func Test_SequentialStrategy(t *testing.T) {
	s := NewSequentialStrategy()

	t.Run("SelectEachHostSequentially", func(t *testing.T) {
		hosts := generateHostMocks(0.9, 0.9, 0.9, 0.8)
		for index, host := range hosts {
			t.Run(fmt.Sprintf("HostNo%d", index), func(t *testing.T) {
				selectedHost, err := s.SelectHost(hosts)
				assert.NoError(t, err)
				assert.Same(t, host, selectedHost)
			})
		}

		t.Run("SelectHost0WhenHitTheLastHost", func(t *testing.T) {
			host, err := s.SelectHost(hosts)
			assert.NoError(t, err)
			assert.Same(t, hosts[0], host)
		})
	})

	t.Run("ErrorOnEmptyList", func(t *testing.T) {
		_, err := s.SelectHost([]Host{})
		assert.ErrorIs(t, err, ErrEmptyListOfHosts)
	})
}

type hostMock struct {
	load  float64
	index int
}

func (m *hostMock) GetLoad() float64      { return m.load }
func (m *hostMock) HandleRequest(Request) {}

func generateHostMocks(loads ...float64) []Host {
	hosts := make([]Host, len(loads))
	for index, load := range loads {
		hosts[index] = &hostMock{load: load, index: index}
	}

	return hosts
}
