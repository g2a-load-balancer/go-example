package main

import "errors"

var (
	ErrEmptyListOfHosts = errors.New("Cannot process further with an empty list of hosts")
)

type Request interface{}

type Host interface {
	GetLoad() float64
	HandleRequest(Request)
}

type LoadBalancerStrategy interface {
	SelectHost([]Host) (Host, error)
}

type LoadBalancer struct {
	hosts    []Host
	strategy LoadBalancerStrategy
	load     float64
}

func NewLoadBalancer(
	hosts []Host,
	strategy LoadBalancerStrategy,
) *LoadBalancer {
	return &LoadBalancer{
		hosts:    hosts,
		strategy: strategy,
	}
}

func (l *LoadBalancer) GetLoad() float64 { return l.load }

func (l *LoadBalancer) HandleRequest(r Request) {
	host, err := l.strategy.SelectHost(l.hosts)
	if err != nil {
		return
	}

	host.HandleRequest(r)
}

type LoadUnderStragegy struct {
	load float64
}

func NewLoadUnderStrategy(maximumLoad float64) *LoadUnderStragegy {
	return &LoadUnderStragegy{load: maximumLoad}
}

func (s *LoadUnderStragegy) SelectHost(hosts []Host) (Host, error) {
	if len(hosts) == 0 {
		return nil, ErrEmptyListOfHosts
	}

	var withLowestLoad Host = hosts[0]
	for _, host := range hosts {
		if host.GetLoad() < s.load {
			return host, nil
		}
		if host.GetLoad() < withLowestLoad.GetLoad() {
			withLowestLoad = host
		}
	}

	return withLowestLoad, nil
}

type SequentialStrategy struct {
	pointer    int
	lastLength int
}

func NewSequentialStrategy() *SequentialStrategy {
	return &SequentialStrategy{}
}

func (s *SequentialStrategy) SelectHost(hosts []Host) (Host, error) {
	if len(hosts) == 0 {
		return nil, ErrEmptyListOfHosts
	}
	if len(hosts) != s.lastLength {
		s.pointer = 0
		s.lastLength = len(hosts)
	}
	if s.pointer >= len(hosts) {
		s.pointer = 0
	}

	defer func() {
		s.pointer++
	}()

	return hosts[s.pointer], nil
}
